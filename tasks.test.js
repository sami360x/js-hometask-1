const {
    convertCelsiusToFahrenheit,
    convertStringToNumber,
    getNaN,
    createGratitude,
    checkA1,
    checkA2,
    checkA3,
    squaresSum
} = require('./tasks');

describe('Задание №1 convertCelsiusToFahrenheit', () => {
    test('Ноль градусов по цельсию', () => {
        expect(convertCelsiusToFahrenheit(0)).toBe(32);
    });
    test('15 градусов по цельсию', () => {
        expect(convertCelsiusToFahrenheit(15)).toBe(59);
    });
    test('-5 градусов по цельсию', () => {
        expect(convertCelsiusToFahrenheit(-5)).toBe(23);
    });
});

describe('Задание №2 convertStringToNumber', () => {
    test('Строка с числом', () => {
        expect(convertStringToNumber("42")).toBe(42);
    });
    test('Строка с текстом', () => {
        expect(convertStringToNumber("Hello")).toBe(false);
    });
});

describe('Задание №3 getNaN', () => {
    test('isNaN', () => {
        expect(isNaN(getNaN())).toBe(true);
    });
});

describe('Задание №4 createGratitude', () => {
    test('Tommy, оценка 5', () => {
        expect(createGratitude('Tommy', 5)).toBe('Tommy оценил вас на 5 из 5. Спасибо, Tommy!');
    });
    test('Без имени, оценка 5', () => {
        expect(createGratitude(undefined, 5)).toBe('Аноним оценил вас на 5 из 5. Спасибо, Аноним!');
    });
    test('Tommy, без оценки', () => {
        expect(createGratitude('Tommy')).toBe('Tommy оценил вас на 0 из 5. Спасибо, Tommy!');
    });
    test('Без имени и без оценки', () => {
        expect(createGratitude()).toBe('Аноним оценил вас на 0 из 5. Спасибо, Аноним!');
    });
});

describe('Задание №5 checkA', () => {
    describe('checkA1', () => {
        test('77', () => {
            expect(checkA1(77)).toBe(77);
        });
        test('0', () => {
            expect(checkA1(0)).toBe('Всё плохо');
        });
        test('-5', () => {
            expect(checkA1(-5)).toBe(-5);
        });
    });
    describe('checkA2', () => {
        test('77', () => {
            expect(checkA2(77)).toBe(77);
        });
        test('0', () => {
            expect(checkA2(0)).toBe('Всё плохо');
        });
        test('-5', () => {
            expect(checkA2(-5)).toBe(-5);
        });
    });
    describe('checkA3', () => {
        test('77', () => {
            expect(checkA3(77)).toBe(77);
        });
        test('0', () => {
            expect(checkA3(0)).toBe('Всё плохо');
        });
        test('-5', () => {
            expect(checkA3(-5)).toBe(-5);
        });
    });
});


describe('Задание №6 squaresSum', () => {
    test('(5,6)', () => {
        expect(squaresSum(5, 6)).toBe(61);
    });
    test('(1,4)', () => {
        expect(squaresSum(1, 4)).toBe(30);
    });
    test('(-5,4)', () => {
        expect(squaresSum(-5, 4)).toBe(85);
    });
});
